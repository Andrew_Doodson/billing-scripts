Billing Scripts
This is the scripts that are used to clean the CSV's that you can download from NMI and bryte

For the NMI refunds script, make sure that the script and the CSV are in the same folder 
For the PayPal cleaning script make sure that the script and the PayPal CSV are in the same folder. 
For the PayPal find differences script make sure that both of the cleaned CSVs and the script are in the same folder.
 
Theses cleaned CSVs are then uploaded to google sheets and accessed through Tableau. 
