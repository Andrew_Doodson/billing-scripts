# -*- coding: utf-8 -*-
"""
Created on Wed Nov 10 09:41:14 2021

@author: Ruby

This scripts purpose is to clean the closed cases paypal CSVs to be used for billing in HR DTB
"""
import os
import pandas as pd


filepath =  os.path.dirname(__file__) +"\\"

#Open this csv
first_file_name = input("Enter the older files name (without extension): ") + ".csv"
file1 = filepath + first_file_name 
#Open the second file to compare to
second_file_name = input("Input newer files name (without the extension): ") + ".csv"
file2 = filepath+second_file_name


#Do the comparison between the files
with open(file1, 'r') as t1, open(file2, 'r') as t2:

    fileone = t1.readlines()

    filetwo = t2.readlines()

with open('Difference in Closed Cases.csv', 'w') as outFile:
    for line in filetwo:
        if line not in fileone:
            outFile.write(line)
   
#Now add back in the column headers into this diff            

#Establish the filepath and the file that we will be using - this is the directory that the script is in

column_names=['case_type','case_id','original_transaction_id','transaction_date', 'case_reason', 
                'case_filing_date', 'case_status', 'disputed_amount', 'disputed_transaction_id', 'money_movement',
                'settlement_type', 'seller_protection', 'chargeback_reason_code']

filepath =  os.path.dirname(__file__) +"\\"
difference = pd.read_csv(filepath + "Difference in Closed Cases.csv", names = column_names)



#difference = difference.set_axis(column_names, axis =1)
#Save the dataframe to a CSV - which will then be re-opened
difference.to_csv("Cleaned Difference.csv", index=False)

