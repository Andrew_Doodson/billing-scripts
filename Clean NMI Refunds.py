# -*- coding: utf-8 -*-
"""
Created on Mon Nov 22 12:13:50 2021

@author: Ruby
This is to take the refunds Excel from the NMI portal and clean them to use in Tableau
"""
#Imports 
import pandas as pd
import os
#from datetime import datetime


filepath =  os.path.dirname(__file__) +"\\"
file_name = input("Enter the refund file name (without file extension): ") + ".csv"
#Manipulate this as a dataframe to remove the columns that are unneeded
file = pd.read_csv(filepath+file_name)

columns_to_keep = ["id", "reference_id", "type","orderid", "time", "amount","status",
                   "response","email", 'username', 'customer_id']

cleaned_file = file[columns_to_keep]
#Convert the time column from a bunch of numbers into a sensible datetime format
cleaned_file['time'] = pd.to_datetime(cleaned_file['time'], format = '%Y%m%d%H%M%S')

cleaned_file.to_csv("Cleaned " + file_name, index = False)