# -*- coding: utf-8 -*-
"""
Created on Wed Nov 10 15:59:10 2021

@author: Ruby
"""

import pandas as pd
import os

#Establish the filepath and the file that we will be using - this is the directory that the script is in
filepath =  os.path.dirname(__file__) +"\\"

file_name = input("Enter new file name (without extension): ") + ".csv"
#Manipulate this as a dataframe to remove the columns that are unneeded
file = pd.read_csv(filepath+file_name)

#Drop the unused columns
columns_to_drop = ["Claimant name",	"Claimant email address","Transaction invoice ID","Card Type",
                   "Response due date", "Disputed currency", "Seller protection payout amount",
                   "Seller protection currency","Payment Tracking ID","Buyer comments",
                   "Store ID", "Outcome"]

file = file.drop(columns_to_drop, axis =1)

#Rename the columns
column_names=['case_type','case_id','original_transaction_id','transaction_date', 'case_reason', 
              'case_filing_date', 'case_status', 'disputed_amount', 'disputed_transaction_id', 'money_movement',
              'settlement_type', 'seller_protection', 'chargeback_reason_code']

file = file.set_axis(column_names, axis =1)

#Save the dataframe to a CSV - which will then be re-opened
file.to_csv("Cleaned "+ file_name, index = False)


